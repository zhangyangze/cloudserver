/**
 * Created by 404forever.com on 2019/11/3 4:07
 */
package com.zyz.commen.base;

import lombok.Data;

import java.io.Serializable;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/3 4:07
 * @since 0.1.0
 */
@Data
public class BaseResponseResult<T> implements Serializable {
    private static final long serialVersionUID = -2758292089824789967L;

    private Boolean success;

    private String message;

    private T data;

    public BaseResponseResult<T> initSuccess(T data) {
        this.success = true;
        this.data = data;
        this.message = "OK";
        return this;
    }

    public BaseResponseResult<T> initFailure(String message) {
        this.success = false;
        this.message = message;
        return this;
    }

}

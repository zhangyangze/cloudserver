/**
 * Created by 404forever.com on 2019/11/3 4:13
 */
package com.zyz.commen.base;

import com.zyz.commen.exception.ServiceExceptException;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Supplier;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/3 4:13
 * @since 0.1.0
 */
@Slf4j
public abstract class BaseAct {

    private static final String ERROR_MESSAGE = "发生未知错误";

    protected <T> BaseResponseResult<T>  fnc(Supplier<T> deal) {
        BaseResponseResult<T> result = new BaseResponseResult<T>();
        try {
            return result.initSuccess(deal.get());
        } catch (ServiceExceptException e) {
            log.warn("发生错误，原因：", e);
            return result.initFailure(e.getMessage());
        } catch (Exception e) {
            log.warn("发生错误", e);
            return result.initFailure(ERROR_MESSAGE);
        }
    }


}

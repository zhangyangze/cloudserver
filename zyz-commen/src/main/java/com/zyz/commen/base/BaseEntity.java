package com.zyz.commen.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/3 3:02
 * @since 0.1.0
 */
@Getter
@Setter
public class BaseEntity<T> extends Model {
    private static final long serialVersionUID = -2105481020132327745L;
    /**
     * 是否删除
     */
    @TableField("is_del")
    private Boolean isDel = false;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @TableField("modify_time")
    private LocalDateTime modifyTime;

    /**
     * 备注
     */
    @TableField("remarks")
    private String remarks;


}

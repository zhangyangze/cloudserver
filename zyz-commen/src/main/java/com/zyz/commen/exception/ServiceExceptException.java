/***
 * Created by 404forever.com on 2019/11/3 2:55
 *
 */
package com.zyz.commen.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/3 2:55
 * @since 0.1.0
 */
@Slf4j
public class ServiceExceptException extends RuntimeException {
    private static final long serialVersionUID = -964761340187974357L;

    private String message;
    private Throwable cuses;

    public ServiceExceptException(String message) {
        super(message);
    }

    public ServiceExceptException(String message, Throwable cuses) {
        super(message, cuses);
    }


}

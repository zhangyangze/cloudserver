package com.zyz.user.action;

import com.zyz.commen.base.BaseAct;
import com.zyz.commen.base.BaseResponseResult;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/28 23:25
 * @since 0.1.0
 */
@Api(tags = "用户测试控制器")
@RestController
@RequestMapping("/api/test")
public class TestAct extends BaseAct {

    @RequestMapping(value = "/user_feign_test", method = RequestMethod.GET)
    public BaseResponseResult<String> test() {
        return fnc(() -> "test");
    }

}

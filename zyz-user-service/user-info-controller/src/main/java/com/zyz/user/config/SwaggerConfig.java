/***
 * Created by 404forever.com on 2019/11/3 0:09
 *
 */
package com.zyz.user.config;

import org.springframework.beans.factory.annotation.Configurable;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @class com.zyz.user.config.SwaggerConfig
 * @date 2019/11/3 0:09
 * @since 0.1.0
 */
@Configurable
@EnableSwagger2
public class SwaggerConfig {
}

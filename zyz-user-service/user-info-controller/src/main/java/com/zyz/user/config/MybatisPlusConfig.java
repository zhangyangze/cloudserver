/***
 * Created by 404forever.com on 2019/11/2 21:49
 *
 */
package com.zyz.user.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/2 21:49
 * @since 0.1.0
 */
@Configuration
@MapperScan("com.zyz.user.dao")
public class MybatisPlusConfig {


    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor(){
        PaginationInterceptor page = new PaginationInterceptor();
        page.setDialectClazz(DbType.MYSQL.getDb());
        return page;
    }

}

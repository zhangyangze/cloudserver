import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author zhangyange
 * @date 2019/11/1 21:50
 */
@EnableFeignClients(value = "com.zyz.user")
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(value = {"com.zyz.user"})
public class UserInfoApp {

    public static void main(String[] args) {
        SpringApplication.run(UserInfoApp.class, args);
    }
}

package com.zyz.user.qo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhangyange
 * @date 2019/11/1 23:55
 * @since 0.1.0
 */
@Data
public class TestQo implements Serializable {
    private static final long serialVersionUID = 6233714529853055479L;
}

/**
 * Created by 404forever.com on 2019/11/3 3:50
 */
package com.zyz.product.manager.pm.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyz.product.dao.pm.PmProductInfoDao;
import com.zyz.product.entity.pm.PmProductInfo;
import com.zyz.product.manager.pm.IPmProductInfoManager;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/3 3:50
 * @since 0.1.0
 */
@Service
public class PmProductInfoManagerImpl extends ServiceImpl<PmProductInfoDao, PmProductInfo> implements IPmProductInfoManager {
    @Override
    public List<PmProductInfo> findAll() {
        return baseMapper.findAll(false);
    }
}

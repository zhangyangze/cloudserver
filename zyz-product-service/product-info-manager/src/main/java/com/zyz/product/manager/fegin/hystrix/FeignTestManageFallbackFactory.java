package com.zyz.product.manager.fegin.hystrix;

import com.zyz.commen.base.BaseResponseResult;
import com.zyz.product.manager.fegin.second.IFeignTestManager;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.zyz.product.manager.fegin.contant.StaticFinalPool.SERVER_HYSTIRX_ERROR_MESSAGE;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/28 23:46
 * @since 0.1.0
 */
@Slf4j
@Component
public class FeignTestManageFallbackFactory implements FallbackFactory<IFeignTestManager> {
    @Override
    public IFeignTestManager create(Throwable cause) {
        return new IFeignTestManager() {

            @Override
            public BaseResponseResult<String> test() {
                log.warn("熔断1", cause);
                return new BaseResponseResult<String>().initFailure(SERVER_HYSTIRX_ERROR_MESSAGE);
            }
        };
    }
}

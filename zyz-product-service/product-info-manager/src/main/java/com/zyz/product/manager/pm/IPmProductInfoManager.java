/**
 * Created by 404forever.com on 2019/11/3 3:49
 */
package com.zyz.product.manager.pm;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyz.product.entity.pm.PmProductInfo;

import java.util.List;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/3 3:49
 * @since 0.1.0
 */
public interface IPmProductInfoManager extends IService<PmProductInfo> {

    /**
     * <p>查询所有</p>
     *
     * @author zhang yang ze
     * @return 商品集合
     * @since 0.1.0
     */
    List<PmProductInfo> findAll();

}

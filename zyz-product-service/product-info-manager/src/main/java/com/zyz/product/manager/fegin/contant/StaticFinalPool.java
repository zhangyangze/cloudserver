package com.zyz.product.manager.fegin.contant;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/28 23:21
 * @since 0.1.0
 */
public final class StaticFinalPool {
    public static final String SERVER_HYSTIRX_ERROR_MESSAGE = "无法连接用户服务，请练习管理员";
}

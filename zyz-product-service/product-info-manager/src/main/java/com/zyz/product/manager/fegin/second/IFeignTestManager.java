package com.zyz.product.manager.fegin.second;

import com.zyz.commen.base.BaseResponseResult;
import com.zyz.product.manager.fegin.hystrix.FeignTestManageFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/28 23:38
 * @since 0.1.0
 */
@FeignClient(name = "user-info-service", fallbackFactory = FeignTestManageFallbackFactory.class)
public interface IFeignTestManager {

    @RequestMapping(value = "/api/test/user_feign_test", method = RequestMethod.GET)
    BaseResponseResult<String> test();

}

/**
 * Created by 404forever.com on 2019/11/3 3:20
 */
package com.zyz.product.entity.pm;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zyz.commen.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/3 3:20
 * @since 0.1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("pm_product_info")
public class PmProductInfo extends BaseEntity<PmProductInfo> {
    private static final long serialVersionUID = -5138342062118584646L;

    @TableId(value = "pi_id", type = IdType.AUTO)
    private Long piId;

    /**
     * 商品编码
     */
    @TableField("pi_code")
    private String piCode;

    /**
     * 商品名称
     */
    @TableField("pi_name")
    private String piName;

}

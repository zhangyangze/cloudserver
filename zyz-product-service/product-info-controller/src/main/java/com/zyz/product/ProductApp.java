package com.zyz.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/3 2:34
 * @since 0.1.0
 */
@EnableFeignClients(value = "com.zyz.product")
@EnableSwagger2
@EnableDiscoveryClient
@ComponentScan(value = "com.zyz")
@SpringBootApplication
public class ProductApp {

    public static void main(String[] args) {
        SpringApplication.run(ProductApp.class, args);
    }

}

/**
 * Created by 404forever.com on 2019/11/3 4:04
 */
package com.zyz.product.controller.sys;

import com.zyz.commen.base.BaseAct;
import com.zyz.commen.base.BaseResponseResult;
import com.zyz.product.entity.pm.PmProductInfo;
import com.zyz.product.service.pm.IPmProductInfoService;
import com.zyz.product.manager.fegin.second.IFeignTestManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/3 4:04
 * @since 0.1.0
 */
@Api(tags = "商品管理服务")
@RestController
@RequestMapping("/product/product_info")
public class SysProductInfoAct extends BaseAct {

    /**
     * <p>查询所有</p>
     *
     * @return 列表
     * @author zhang yang ze
     * @date 2019/11/3 4:29
     * @since 0.1.0
     */
    @ApiOperation("查询所有")
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public BaseResponseResult<List<PmProductInfo>> getAll() {
        return fnc(() -> service.findAll());
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public BaseResponseResult<String> test() {
        return feignTestManager.test();
    }

    @Autowired
    private IPmProductInfoService service;
    @Autowired
    private IFeignTestManager feignTestManager;
}

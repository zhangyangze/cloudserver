/**
 * Created by 404forever.com on 2019/11/3 3:59
 */
package com.zyz.product.service.pm;

import com.zyz.product.entity.pm.PmProductInfo;

import java.util.List;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/3 3:59
 * @since 0.1.0
 */
public interface IPmProductInfoService {

    /**
     * <p>查询所有</p>
     *
     * @return 商品集合
     * @author zhang yang ze
     * @since 0.1.0
     */
    List<PmProductInfo> findAll();

}

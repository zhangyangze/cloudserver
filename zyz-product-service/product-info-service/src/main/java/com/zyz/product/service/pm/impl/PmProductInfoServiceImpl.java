/**
 * Created by 404forever.com on 2019/11/3 4:00
 */
package com.zyz.product.service.pm.impl;

import com.zyz.product.entity.pm.PmProductInfo;
import com.zyz.product.manager.pm.IPmProductInfoManager;
import com.zyz.product.service.pm.IPmProductInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/3 4:00
 * @since 0.1.0
 */
@Slf4j
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class PmProductInfoServiceImpl implements IPmProductInfoService {
    @Override
    public List<PmProductInfo> findAll() {
        return manager.findAll();
    }

    @Autowired
    private IPmProductInfoManager manager;
}

/**
 * Created by 404forever.com on 2019/11/3 3:34
 */
package com.zyz.product.dao.pm;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyz.product.entity.pm.PmProductInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p></p>
 *
 * @author zhang yang ze
 * @date 2019/11/3 3:34
 * @since 0.1.0
 */
public interface PmProductInfoDao extends BaseMapper<PmProductInfo> {

    /**
     * <p>查询所有</p>
     *
     * @param isDel 是否删除
     * @author zhang yang ze
     * @return 商品集合
     * @since 0.1.0
     */
    List<PmProductInfo> findAll(@Param("isDel") Boolean isDel);

}

package com.zyz.zuulsever;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author zhang yang ze
 */
@EnableZuulProxy
@EnableDiscoveryClient
@SpringBootApplication
public class ZuulSeverApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZuulSeverApplication.class, args);
    }

}
